.. this file should at least contain the root `toctree` directive.

Welcome!
====================

Personal notes and walkthroughs on vulnerable machines!

.. Table of Contents
.. --------------------

.. toctree::
   :hidden:
   :caption: GENERAL
   :maxdepth: 2

   Connecting <general/connecting.rst>

.. toctree::
   :hidden:
   :caption: HACK THE BOX
   :maxdepth: 2

   STARTING POINT <htb/starting_point/index.rst>
   MACHINES <htb/machines/index.rst>
   CHALLENGES <htb/challenges/index.rst>

.. toctree::
   :hidden:
   :caption: VULNHUB
   :maxdepth: 2

.. toctree::
   :hidden:
   :caption: PROVING GROUNDS
   :maxdepth: 2